#!/bin/bash

thisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

printf "Compiling typescript files... "
"${thisDir}/node_modules/.bin/tsc" --sourcemap "@tsc-files.txt" --out ${thisDir}/public/js/script.js --removeComments

printf "uglifying... "
"${thisDir}/node_modules/.bin/uglifyjs" "${thisDir}/public/js/script.js" -o "${thisDir}/public/js/script.min.js" --compress --mangle

printf "finished.\n"
