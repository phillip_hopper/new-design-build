#!/usr/bin/env bash

thisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ext=".css"
path="public/css"

for file in ${thisDir}/less/*.less
do
	modified=`stat -c "%Y" ${file}`

    destination="${file/.less/$ext}"
    destination="${destination/less/$path}"

    "${thisDir}/node_modules/.bin/lessc" --include-path=${thisDir}/less/include --clean-css="--s1 -b" --no-color "$file" "$destination"
    echo "$destination"

done
echo "finished"
