# New Design Build

__ABSOLUTELY NO SECURITY CREDENTIALS ARE TO BE CHECKED INTO THIS REPOSITORY!__

### Setting up a development environment

1. Clone this repository.
2. Open a terminal and `cd` to the directory where you cloned the repository. Alternately you could create a project with IntelliJ IDEA and run the following commands in the IntelliJ project terminal.
3. Install the node libraries.
    ```
    npm install
    ```
    
### Compiling LESS files

```bash
./less.sh
```

### Compiling TypeScript files

```bash
./tsc.sh
```

### Deploying to test server

_Instructions coming soon._
